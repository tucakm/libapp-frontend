import React, { Fragment, useContext, useEffect, useState } from "react";
import { Button, Form, Header, Segment } from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import { SelectInput } from "../formComponents/SelectInput";
import { contactOptions } from "../constants/contactOptions";
import { TextInput } from "../formComponents/TextInput";
import { observer } from "mobx-react-lite";
import { RootStoreContext } from "../stores/rootStore";
import { combineValidators, isRequired } from "revalidate";
interface IProps {
  userId: string;
}
const validate = combineValidators({
    type: isRequired({ message: "Type is required" }),
    value: isRequired({ message: "Value is required" }),   
  });
const ContactForm: React.FC<IProps> = ({ userId }) => {
  const rootStore = useContext(RootStoreContext);
  const { createUserContact, submiting, loadUser, user } = rootStore.userStore;
  const [anotherContact, setAnotherContact] = useState(false);

  useEffect(() => {
    loadUser(userId);
  }, [loadUser, userId]);

  const handleFinalFormSubmit = (values: any) => {
    createUserContact(user!.id, values).then(() => setAnotherContact(true));
  };
  const handleAddingAnotherContact = () => {
    setAnotherContact(false);
  };

  return (
    <Fragment>
     
      <FinalForm
        validate={validate}
        onSubmit={handleFinalFormSubmit}
        
        render={({ handleSubmit, invalid, pristine, }) => (
          <Form onSubmit={handleSubmit} >
            <Header as="h1">Add Contact for user</Header>
            <Field
              name="type"
              options={contactOptions}
              placeholder="Type"
              component={SelectInput}
            />
            <Field name="value" placeholder="Value" component={TextInput} />
            <Button
              loading={submiting}
              disabled={invalid || pristine}
              floated="right"
              positive
              type="submit"
              content="Submit"
            />
          </Form>
        )}
      />

      {anotherContact && (
        <Segment>
          <Button onClick={() => handleAddingAnotherContact()} negative>
            Another contact
          </Button>
        </Segment>
      )}
    </Fragment>
  );
};

export default observer(ContactForm);
