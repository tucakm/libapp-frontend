import React, { useContext, useEffect, useState } from "react";
import {
  Grid,
  GridColumn,
  Segment,
  Button,
  Form,
  Header,
} from "semantic-ui-react";
import { Form as FinalForm, Field } from "react-final-form";
import { v4 as uuid } from "uuid";
import { TextInput } from "../formComponents/TextInput";
import { DateInput } from "../formComponents/DateInput";
import { RootStoreContext } from "../stores/rootStore";
import { combineValidators, isRequired } from "revalidate";
import ContactForm from "./ContactForm";
import { IUser, IUserContacts } from "../models/user";

const validate = combineValidators({
  firstName: isRequired({ message: "First Name is required" }),
  lastName: isRequired({ message: "Last Name is required" }),
  birthDay: isRequired({ message: "Birth day is required" }),
});
const UserForm: React.FC = () => {
  const rootStore = useContext(RootStoreContext);
  const { createUser, submiting } = rootStore.userStore;
  const [contactForm, setAvaibleContactForm] = useState(false);
  const [user, setUser] = useState<IUser>();

  const handleFinalFormSubmit = (values: any) => {
    const { ...user } = values;
    if (!user?.id) {
      let newUser = {
        ...user,
        id: uuid(),
      };
      createUser(newUser)
        .then(() => setUser(newUser))
        .then(() => setAvaibleContactForm(true));
    }
  };

  return (
    <Grid centered style={{ marginTop: "10em" }}>
      <GridColumn width={8}>
        <Segment clearing>
      
          <FinalForm
            validate={validate}
            onSubmit={handleFinalFormSubmit}
            render={({ handleSubmit, invalid, pristine }) => (
              <Form onSubmit={handleSubmit}>
                <Header as="h1">Enter user information</Header>
                <Field
                  name="firstName"
                  placeholder="First Name"
                  component={TextInput}
                  readonly={contactForm}
                />
                <Field
                  name="lastName"
                  placeholder="Last name"
                  component={TextInput}
                  readonly={contactForm}
                />
                <Field
                  name="birthDay"
                  placeholder="BirthDay"
                  component={DateInput}
                  date={true}
                  readonly={contactForm}
                  maxDate={new Date()}
                />

                <Button
                  loading={submiting}
                  disabled={invalid || pristine || contactForm}
                  floated="right"
                  positive
                  type="submit"
                  content="Submit"
                />
              </Form>
            )}
          />
        </Segment>
      </GridColumn>

      {contactForm && (
        <GridColumn width={6}>
          <Segment>
            <ContactForm userId={user!.id} />
          </Segment>
        </GridColumn>
      )}
    </Grid>
  );
};

export default UserForm;
