import React, { Fragment } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import UserForm from "../features/UserForm";

const App: React.FC = () => {
  return (
    <Fragment>
      <ToastContainer position="bottom-right" />
      <Switch>
        <Redirect from="/home" to="/" />
        <Route exactPath="/" component={UserForm} />
      </Switch>
    </Fragment>
  );
};

export default App;
