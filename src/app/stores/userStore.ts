import { RootStore } from "./rootStore";
import { observable, action, runInAction } from "mobx";
import { IUser, IUserContacts } from "../models/user";
import agent from "../api/agent";
import { toast } from "react-toastify";
export default class UserStore {
  rootStore: RootStore;
  constructor(Rootstore: RootStore) {
    this.rootStore = Rootstore;
  }
  @observable userRegistry = new Map();
  @observable user: IUser | null = null;
  @observable submiting = false;
  @observable loading = false;
  @action createUser = async (user: IUser) => {
    this.submiting = true;
    try {
      await agent.User.create(user);
      runInAction(() => {
        this.user = user;
        this.userRegistry.set(user.id, user);
        this.submiting = false;
        toast.success("Added new user");
      });
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      toast.error("Problem submitting data");
      console.log(error.response);
    }
  };
  @action createUserContact = async (userId: string, contact: IUserContacts) => {
    this.submiting = true;
    contact.type=Number.parseInt(contact.type);
    try {
      await agent.User.creatContact(userId, contact);
      
      this.submiting = false;
      toast.success("Added new contact for user");
    } catch (error) {
      runInAction(() => {
        this.submiting = false;
      });
      toast.error("Problem submiting data");
      console.log(error.response);
    }
  };
  @action loadUser = async (id: string) => {
    this.loading = true;
    try {
      let user = await agent.User.get(id);
      runInAction(() => {
        this.user = user;
        this.userRegistry.set(user.id, user);
        this.loading = false;
      });
      return this.user;
    } catch (error) {
      runInAction(() => {
        this.loading = false;
      });
    }
  };
}
