export interface IUser {
  id: string;
  firstName: string;
  lastName: string;
  birthDay: Date;
  userContacts?: IUserContacts[];
}
export interface IUserContacts {
    id:string;
    type:any;
    value:string;
}
